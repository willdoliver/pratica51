/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author lucas
 */
public class ProdMatrizesIncompativeisException extends MatrizesIncompativeisException {

    private final Matriz m1;
    private final Matriz m2;
    private final String mensagem;

    public ProdMatrizesIncompativeisException(Matriz m1, Matriz m2, String mensagem) {
        super(m1, m2, mensagem);
        this.mensagem = mensagem;
        this.m1 = m1;
        this.m2 = m2;
    }

}


          