/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author lucas
 */
public class MatrizInvalidaException extends Exception {

    int m, n;

    public MatrizInvalidaException(int m, int n) {
        super("Matriz de" + m + "x" + n + " nao pode ser criada");     
        this.m = m;
        this.n = n;
    }

    public int getNumLinhas() {
        return m;
    }

    public int getNumColunas() {
        return n;
    }

}
